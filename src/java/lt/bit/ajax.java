/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bit;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Fractal
 */
@Path("json")
public class ajax {
    
    
    @GET
    @Path("people")
    @Produces(MediaType.APPLICATION_JSON)
    public List<People> getPeople(){
        EntityManager em = EMF.getEntityManager();
        Query q = em.createQuery("select p from People p");
        List<People> result = q.getResultList();
        EMF.returnEntityManager(em);
        return result;
                
    }
    
    @DELETE
    @Path("people/{id}")
    public void deletePerson(@PathParam("id") Integer id) throws Exception{
        EntityManager em = EMF.getEntityManager();
        EntityTransaction tx = EMF.getTransaction(em);
        People p = em.find(People.class, id);
        em.remove(p);
        EMF.commitTransaction(tx);
        EMF.returnEntityManager(em);
    }
    
    
}
