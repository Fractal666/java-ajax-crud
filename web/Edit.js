/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
let id = window.location.href.split("p=")[1];
$(document).ready(function () {
    let id = window.location.href.split("id=")[1];


    request = $.ajax({url: "rest/json/people/" + id, method: "GET"});
    request.done(response => {
        $("#peopleName").val(response.firstName);
        $("#peopleSurname").val(response.lastName);
        $("#peopleBirthDate").val(response.birthDate.split("T")[0]);
        $("#peopleSalary").val(response.salary);
        console.log(response);

    });
});
$(".ok").click(function () {
    let id = window.location.href.split("id=")[1];
    let name = $("#peopleName").val();
    let surname = $("#peopleSurname").val();
    let birthDate = $("#peopleBirthDate").val();
    let salary = $("#peopleSalary").val();

    let data = {
        firstName: name,
        lastName: surname,
        birthDate: birthDate + "T00:00:00Z[UTC]",
        salary: salary
    };


    if (id == undefined) {
        request = $.ajax({url: "rest/json/people", method: "POST", data: JSON.stringify(data), contentType: "application/json; charset=utf-8", dataType: "json"});
        request.done(response => {
            window.location.href = "index.html";
        });
    } else {

        request = $.ajax({url: "rest/json/people/" + id, method: "PUT", data: JSON.stringify(data), contentType: "application/json; charset=utf-8", dataType: "json"});
        request.done(response => {
            window.location.href = "index.html";
        });
    }

});

$(".cancel").click(function () {

    window.location.href = "index.html";
});

